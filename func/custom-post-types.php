<?php

  // ADDING CUSTOM POST TYPE
  add_action('init', 'all_custom_post_types');
 
  function all_custom_post_types() {
 
    $types = array(
 
      // Banners
      array('the_type'  => 'banner',
            'single'    => 'Banner',
            'plural'    => 'Banners'),
 
    );
 
    foreach ($types as $type) {
 
      $the_type = $type['the_type'];
      $single   = $type['single'];
      $plural   = $type['plural'];
 
      $labels = array(
        'name'                => _x($plural, 'post type general name'),
        'singular_name'       => _x($single, 'post type singular name'),
        'add_new'             => _x('Adicionar', $single),
        'add_new_item'        => __('Adicionar '. $single),
        'edit_item'           => __('Editar '.$single),
        'new_item'            => __('Novo '.$single),
        'view_item'           => __('Ver '.$single),
        'search_items'        => __('Pesquisar '.$plural),
        'not_found'           =>  __('Não '.$plural.' encontrado'),
        'not_found_in_trash'  => __('Não '.$plural.' encontrado na lixeira'),
        'parent_item_colon'   => ''
      );
 
      $args = array(
        'labels'              => $labels,
        'public'              => true,
        'has_archive'         => true,
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'query_var'           => true,
        'rewrite'             => true,
        'capability_type'     => 'post',
        'hierarchical'        => false,
        'menu_position'       => 5,
        'supports'            => array('title','editor','thumbnail')
      );
 
      register_post_type($the_type, $args);
 
    }
 
  }


/* -------------------------------------------
    CUSTOMIZAÇÕES
   ------------------------------------------- */


// REGISTRANDO TAXONOMIA
register_taxonomy("Categorias", array("banner"), array("hierarchical" => true, "label" => "Categorias", "singular_label" => "Categorias", "rewrite" => true));



// ADICIONA METABOX 
add_action("admin_init", "admin_init");
 
function admin_init(){
  add_meta_box("link_banner-meta", "Link", "link_banner", "banner", "normal", "low");
}
 
function link_banner(){
  global $post;
  $custom = get_post_custom($post->ID);
  $link_banner = $custom["link_banner"][0];
  ?>
  <input name="link_banner" value="<?php echo $link_banner; ?>"  style="width: 500px;" placeholder="Ex.: http://" />
  <?php
}
 


// SALVAR OS METABOX
add_action('save_post', 'save_details');

function save_details(){
  global $post;
  update_post_meta($post->ID, "link_banner", $_POST["link_banner"]);
}



// ADICIONA COLUNAS
// BANNER
add_action("manage_posts_custom_column", "banner_custom_columns");
add_filter("manage_edit-banner_columns", "banner_edit_columns");
 
function banner_edit_columns($columns){
  $columns = array(
    "cb"              => "<input type=\"checkbox\" />",
    "title"           => "Título",
    "conteudo_banner" => "Conteúdo",
    "link"            => "Link",
    "categorias"      => "Categorias",
  );
 
  return $columns;
}

function banner_custom_columns($column){
  global $post;
 
  switch ($column) {
    case "conteudo_banner":
      the_excerpt();
      break;
    case "link":
      $custom = get_post_custom();
      echo $custom["link_banner"][0];
      break;
    case "categorias":
      echo get_the_term_list($post->ID, 'Categorias', '', ', ','');
      break;
  }
}



// ALTERAR ICONES
add_action( 'registered_post_type', 'alterar_icones', 10, 2 );
function alterar_icones( $post_type, $args ) {
    if ( 'banner' === $post_type ) {
        global $wp_post_types;
        $args->menu_icon = __( 'dashicons-format-image', 'banner' );
        $wp_post_types[ $post_type ] = $args;
    }

}


?>