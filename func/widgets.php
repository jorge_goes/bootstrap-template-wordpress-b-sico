<?php

function widgets_init() {

  	/*
    Lateral
     */
    register_sidebar( array(
        'name' => __( 'Lateral', 'pms' ),
        'id' => 'lateral-widget-area',
        'description' => __( 'Área para widgets lateral', 'pms' ),
        'before_widget' => '<section class="%1$s %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );


}
add_action( 'widgets_init', 'widgets_init' );

?>