<?php

/*
Google CDN jQuery with a local fallback
See http://www.wpcoke.com/load-jquery-from-cdn-with-local-fallback-for-wordpress/
*/
// if( !is_admin()){ 
//     $url = 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'; 
//     $test_url = @fopen($url,'r'); 
//     if($test_url !== false) { 
//         function load_external_jQuery() {
//             wp_deregister_script('jquery'); 
//             wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'); 
//             wp_enqueue_script('jquery'); 
//         }
//         add_action('wp_enqueue_scripts', 'load_external_jQuery'); 
//     } else {
//         function load_local_jQuery() {
//             wp_deregister_script('jquery'); 
//             wp_register_script('jquery', get_bloginfo('template_url').'/js/jquery-1.11.1.min.js', __FILE__, false, '1.11.1', true); 
//             wp_enqueue_script('jquery'); 
//         }
//     add_action('wp_enqueue_scripts', 'load_local_jQuery'); 
//     }
// }

function enqueues()
{
	wp_register_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', false, null);
	wp_enqueue_style('bootstrap-css');

	wp_register_style('style-css', get_template_directory_uri() . '/style.css', false, null);
	wp_enqueue_style('style-css');

//	wp_register_style('home-css', get_template_directory_uri() . '/css/home.css', false, null);
//	wp_enqueue_style('home-css');

	wp_register_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', false, null, true);
	wp_enqueue_script('bootstrap-js');

}
add_action('wp_enqueue_scripts', 'enqueues', 100);



?>
