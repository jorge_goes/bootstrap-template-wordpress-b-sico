<?php
/* 
Template Name: Sidebar - Esquerda
*/
?>


<?php get_header(); ?>

<?php include 'inc/banner.php' ?>

<div class="div-space-25"></div>

<div class="container">
  <div class="row">
    <div class="col-md-3" style="display: block;" id="sidebar" role="navigation">
      <!-- MENU  -->
      <?php include 'inc/menu-lateral.php';?>
      <!-- ./MENU -->
      <div class="div-space-15"></div>
      <?php dynamic_sidebar('lateral-widget-area'); ?>  
   </div>

    <div class="col-md-9">
      <div id="content" role="main">
        <?php if(have_posts()): while(have_posts()): the_post();?>
        <article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
          <header>
            <h2><?php the_title()?></h2>
            <hr/>
          </header>
          <?php the_content()?>
        </article>
        <?php endwhile; ?> 
        <?php else: ?>
        <?php wp_redirect(get_bloginfo('siteurl').'/404', 404); exit; ?>
        <?php endif;?>
      </div><!-- /#content -->
    </div>
    
  </div><!-- /.row -->

</div><!-- /.container -->

<div class="push"></div>


<div class="clearfix div-space-25"></div>

<div class="bg-dark">
<?php get_footer(); ?>
</div>