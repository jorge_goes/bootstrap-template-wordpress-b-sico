<!-- BANNER -->
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
<!--
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>
-->

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    
					<?php 					
						$count = 0;
						$loop = new WP_Query( array( 'post_type' => 'banner' ) ); 
					  while ( $loop->have_posts() ) : $loop->the_post();
						// URL do Thumbnail
						$thumb_id = get_post_thumbnail_id();
						$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);								
						// echo '<div class="item ' . ( $count == 0 ? 'active' : '') . '">';
					?>    

					<div class="item <?php echo ( $count == 0 ? 'active' : ''); ?>" align="center">
						<a href="<?php echo get_post_meta($post->ID, 'link_banner', true); ?>"><img src="<?php echo $thumb_url[0]; ?>"></a>
					</div>
					
					<?php
						$count++;
						endwhile; wp_reset_query();
					?>  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<!-- ./BANNER -->





