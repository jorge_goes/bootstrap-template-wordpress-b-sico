<?php
/*
All the functions are in the PHP pages in the func/ folder.
*/

require_once locate_template('/func/cleanup.php');
require_once locate_template('/func/setup.php');
//require_once locate_template('/func/enqueues.php');
require_once locate_template('/func/widgets.php');
require_once locate_template('/func/custom-post-types.php');

/* -- Navbar -- */
require_once('js/wp_bootstrap_navwalker.php');
register_nav_menus( array(
'primary' => __( 'Main Menu', 'THEMENAME' ),
) );


/* -- Remove Comments de notícias e pautas -- */
remove_post_type_support( 'blog', 'comments' );



?>